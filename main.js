// 1. Цикли використовуються для повторення певних дій декілька разів
// 2. Цикл "for" використовують, коли потрібно виконати певний код для кожного елемента в масиві
//    Цикл "While" використовується до поки вираження не буде істенним.
//    Цикл "do ... while" схожий на цикл "while" але різниця в тому що код виконується принаймі один раз
// 3. У JavaScript існують два способи приведення типів даних: явне та неявне. Явне приведення типів даних 
//    в JavaScript відбувається за допомогою спеціальних функцій або операторів. Наприклад, використовуючи 
//    функцію "parseInt()" можна явно перетворити рядок у ціле число. Також, існують оператори, які змінюють 
//    тип даних явно, наприклад, оператор + при з'єднанні рядків з числами. Неявне приведення типів даних 
//    відбувається автоматично при виконанні певних операцій. Наприклад, якщо провести операцію додавання 
//    між числом і рядком, JavaScript автоматично перетворить рядок у число

// Отримуєм ціле число від користувача
let inputForm;
do {
  inputForm = prompt("Введіть ціле число!");
} while (!Number.isInteger(Number(inputForm)));

// Вичисляєм всі числа які кратні 5, від 0
const NUMBERS = Number(inputForm);
let multiples = false;
for (let i = 1; i <= NUMBERS; i++) {
    if (i % 5 === 0) {
        console.log(i);
        multiples = true;
    }
}
if (!multiples) {
    console.log("Sorry, no numbers");
}

// Отримуємо числа m і n
let m;
do {
    m = prompt("Введіть ціле число m!");
} while (!Number.isInteger(Number(m)));

let n;
do {
    n = prompt("Введіть ціле число n!");
} while (!Number.isInteger(Number(n)));

// Виводим прості числа
let minNumber = Math.min(m, n);
let maxNumber = Math.max(m, n);
console.log("Prime numbers:")
for (let i = minNumber; i <= maxNumber; i++) {
    if (checkPrime(i)) {
        console.log(i);
    }
}

// Функція для перевірки простих чисел
function checkPrime(number) {
    for (let i = 2; i <= Math.sqrt(number); i++) {
      if (number % i === 0) {
        return false;
      }
    }
    return true;
  }